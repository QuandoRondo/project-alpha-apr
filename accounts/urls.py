from django.urls import path
from accounts.views import account_login, Account_Sign_Up
from django.shortcuts import redirect
from django.contrib.auth import logout


def account_logout(request):
    logout(request)
    return redirect("login")


urlpatterns = [
    path("login/", account_login, name="login"),
    path("logout/", account_logout, name="logout"),
    path("signup/", Account_Sign_Up, name="signup"),
]

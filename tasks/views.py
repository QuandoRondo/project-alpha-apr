from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from tasks.forms import CreateTaskForm
from tasks.models import Task

# Create your views here.


@login_required
def create_task(request):
    if request.method == "POST":
        form = CreateTaskForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")

    else:
        form = CreateTaskForm()

    context = {"form": form}
    return render(request, "tasks/tasks_list.html", context)


@login_required
def show_my_tasks(request):
    list_tasks = Task.objects.all()
    context = {"list_tasks": list_tasks}
    return render(request, "tasks/show_my_tasks.html", context)
